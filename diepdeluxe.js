// ==UserScript==
// @name        Collab Script
// @include	*://diep.io/*
// @author      Actual Developers
// @description Lets do this!
// @connect	diep.io
// @version     68
// @grant       GM_getValue
// @grant       GM_setValue
// @namespace   Diep.io
// ==/UserScript==


//TODO
//Clump, multibox toggle
//Make script compatible with lan session sharing (huehanaejistla)
//DRM protection, script needs to connect to external server once in a while once verified (it would use the same login credentials the user put in to verify first time)
//Split the script in to multiple scripts (child parent system) with their own obfuscation method to make it impossible to reverse engineer it
//This means it needs to frequently verify its continued operation (ping the login session server with user once in a while, ignores it it gets no response only takes action if user
//Has been deactivated during the active script session)

//Afk mode, when engaged none of the bots move. they freeze in their current location and move around in random small patterns to prevent bumping by angry team mates
//Spin fix, C+E+Clump does weird things with the bots, we need to compensate for that.
//Doughnut mode, If the main tank spins the slave tanks will travel in a circle around the master tank like a Doughnut (Perfect if you are multiboxing trappers for protection)
//Auto farm mode, Slave tanks automatically aim for their own shapes to level up so that you can level up your swarm faster

//Headless mode, slaves will not render unneccisary data from diep.io like bullets and other tanks, only shapes to shoot at if afk mode is enabled
//Feeder mode, automatic bots that levels up and shoots nearest shapes and players while moving towards you. stops shooting when they're close to you
//From master tank window, add support to pre program tank levels before farming. using diep console command for automatic stat uppgrades.

//add support to pre program individual tank classes before they level up to make it all go automatically.
